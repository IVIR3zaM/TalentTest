<?php

//use Phalcon\Mvc\Model\Metadata\Files as MetaDataAdapter;
use Phalcon\Mvc\Model\Metadata\Redis as MetaDataAdapter;
use IVIR3zaM\TalentTest\Threads\Manager as ThreadsManager;

$di->setShared('config', function () {
    return require __DIR__ . '/config.php';
});

$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter(/*array(
        'metaDataDir' => dirname(__DIR__) . '/metaData/'
    )*/);
});

$di->setShared('db', function () use ($di) {
    $config = $di->get('config');
    $class = '\Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
    return new $class([
        'host' => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname,
        'charset' => $config->database->charset,
        'persistent' => true,
        'options' => $config->database->options->toArray(),
    ]);
});

$di->setShared('queue', function () use ($di) {
    $config = $di->get('config');
    $class = $config->queue->class;
    $params = $config->queue->params;
    return new $class($params);
});

$di->setShared('thread', function () use ($di) {
    $config = $di->get('config');
    $manager = new ThreadsManager();
    $manager->setMaxLoad($config->threads->maxLoad);
    return $manager;
});

return $di;