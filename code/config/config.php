<?php
use \Phalcon\Config;
use \IVIR3zaM\TalentTest\Queue\Redis;

return new Config([
    'database' => [
        'adapter' => 'Mysql',
        'host' => 'localhost',
        'username' => 'root',
        'password' => '',
        'dbname' => 'samtt',
        'charset' => 'utf8',
        'options' => [],
    ],
    'queue' => [
        'class' => Redis::class,
        'params' => [
            'host' => '127.0.0.1',
            'port' => 6379,
            'list' => 'MoList',
        ],
    ],
    'threads' => [
        'maxLoad' => 500,
    ],
]);