<?php
namespace IVIR3zaM\TalentTest\Tests\Controllers;

use IVIR3zaM\TalentTest\Controllers\AbstractJsonOutputController;

/**
 * Class JsonOutputController
 * A holder for testing AbstractJsonOutputController class
 * @package IVIR3zaM\TalentTest\Tests\Controllers
 */
class JsonOutputController extends AbstractJsonOutputController
{
    /**
     * @param array $output
     */
    public function testAction($output)
    {
        $this->getOutput()->setFromInput($output);
    }
}