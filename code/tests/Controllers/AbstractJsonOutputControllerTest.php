<?php
namespace IVIR3zaM\TalentTest\Tests\Controllers;

use Phalcon\Test\UnitTestCase as PhalconTestCase;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Router;
use Phalcon\Http\Response;

/**
 * Class AbstractJsonOutputControllerTest
 * @package IVIR3zaM\TalentTest\Tests\Controllers
 */
class AbstractJsonOutputControllerTest extends PhalconTestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->getDI()->reset();

        $this->getDI()->setShared('response', function () {
            return new Response();
        });

        $this->getDI()->setShared('dispatcher', function () {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace('IVIR3zaM\TalentTest\Tests\Controllers');
            return $dispatcher;
        });
    }

    /**
     * Testing the output of AbstractJsonOutputController class
     */
    public function testOutput()
    {
        $input = ['Foo' => 'Bar'];
        $response = $this->callController('json_output', 'test', [$input]);
        $this->assertEquals(json_encode($input), $response);
    }

    /**
     * Calling a controller action based on given params and return the output
     *
     * @param string $controller controller name
     * @param string $action action name
     * @param array $params
     * @return string the output of the action
     */
    private function callController($controller, $action, $params = [])
    {
        $dispatcher = $this->getDI()->get('dispatcher');
        $dispatcher->setControllerName($controller);
        $dispatcher->setActionName($action);
        $dispatcher->setParams($params);
        ob_start();
        $dispatcher->dispatch();
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}