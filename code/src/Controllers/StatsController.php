<?php
namespace IVIR3zaM\TalentTest\Controllers;

use IVIR3zaM\TalentTest\Reporters\MoReporter;
use IVIR3zaM\TalentTest\Reporters\MoReporterInterface;
use IVIR3zaM\TalentTest\Models\MoModel;
use Phalcon\Mvc\ModelInterface;
use Phalcon\Http\ResponseInterface;
use DateTime;

/**
 * Class StatsController
 * @package IVIR3zaM\TalentTest\Controllers
 */
class StatsController extends AbstractJsonOutputController
{
    /**
     * Send a summary of the system stats to the output
     * @params ModelInterface $model
     * @params MoReporterInterface $reporter
     * @return ResponseInterface
     */
    public function summaryAction(ModelInterface $model = null, MoReporterInterface $reporter = null)
    {
        if (!$model) {
            $model = new MoModel();
        }
        if ($reporter) {
            $reporter->setModel($model);
        } else {
            $reporter = new MoReporter($model);
        }
        $this->getOutput()->last_15_min_mo_count = $reporter->getLastMoCount(new DateTime('15 minutes ago'));
        $this->getOutput()->time_span_last_10k = $reporter->getTimeSpan(10000);
        return $this->initResponse();
    }
}