<?php
namespace IVIR3zaM\TalentTest\Controllers;

use Phalcon\Http\RequestInterface;
use IVIR3zaM\TalentTest\Mo;
use IVIR3zaM\TalentTest\Queue\QueueInterface;
use Phalcon\Http\ResponseInterface;

/**
 * Class MoController
 * @package IVIR3zaM\TalentTest\Controllers
 */
class MoController extends AbstractJsonOutputController
{
    /**
     * Receive an Mo from request params and push that into th queue
     * @param RequestInterface|null $request
     * @param QueueInterface|null $queue
     * @return ResponseInterface
     */
    public function receiveAction(RequestInterface $request = null, QueueInterface $queue = null)
    {
        if (!$request) {
            $request = $this->getDI()->get('request');
        }

        if (!$queue) {
            $queue = $this->getDI()->get('queue');
        }

        $queue->push(new Mo($request->get()));

        $this->getOutput()->status = 'Ok';
        return $this->initResponse();
    }
}