<?php
namespace IVIR3zaM\TalentTest\Models;

use Phalcon\Mvc\Model;
use IVIR3zaM\TalentTest\MoTrait;

/**
 * Class Mo
 * This Mo Model class and used to save Mo into database and interact with database
 * @package IVIR3zaM\TalentTest\Models
 */
class MoModel extends Model implements MoModelInterface
{
    use MoTrait, MoModelTrait;

    /**
     * @return string the name of Mo table name in the database
     */
    public function getSource()
    {
        return 'mo';
    }
}