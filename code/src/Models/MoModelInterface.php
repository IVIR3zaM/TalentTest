<?php
namespace IVIR3zaM\TalentTest\Models;

use IVIR3zaM\TalentTest\MoInterface;
use DateTime;

/**
 * Interface MoModelInterface
 * @package IVIR3zaM\TalentTest\Models
 */
interface MoModelInterface extends MoInterface
{
    public function getId();

    /**
     * @return string
     */
    public function getAuthToken();

    /**
     * @param string $token
     * @return $this
     */
    public function setAuthToken($token);

    /**
     * @return DateTime
     */
    public function getCreatedAt();

    /**
     * @param DateTime $date
     * @return $this
     */
    public function setCreatedAt(DateTime $date);

    /**
     * @param MoInterface $mo
     * @param string $token
     * @return MoModelInterface
     */
    public static function convertToMoModel(MoInterface $mo, $token);
}