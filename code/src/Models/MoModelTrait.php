<?php
namespace IVIR3zaM\TalentTest\Models;

use IVIR3zaM\TalentTest\MoInterface;
use DateTime;

trait MoModelTrait
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $auth_token;

    /**
     * @var string
     */
    protected $created_at;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAuthToken()
    {
        return $this->auth_token;
    }

    /**
     * @param string $token
     * @return $this
     */
    public function setAuthToken($token)
    {
        $this->auth_token = (string) $token;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return new DateTime($this->created_at);
    }

    /**
     * @param DateTime $date
     * @return $this
     */
    public function setCreatedAt(DateTime $date)
    {
        $this->created_at = $date->format('Y/m/d H:i:s');
        return $this;
    }

    /**
     * @param MoInterface $mo
     * @param string $token
     * @return MoModelInterface
     */
    public static function convertToMoModel(MoInterface $mo, $token)
    {
        $model = new self();
        $model->setFromArray($mo->getArray());
        $model->setAuthToken($token);
        $model->setCreatedAt(new DateTime());
        $model->save();
        return $model;
    }
}