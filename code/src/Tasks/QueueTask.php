<?php
namespace IVIR3zaM\TalentTest\Tasks;

use Phalcon\Cli\Task;
use IVIR3zaM\TalentTest\Queue\QueueInterface;
use IVIR3zaM\TalentTest\Threads\ManagerInterface;
use IVIR3zaM\TalentTest\MoInterface;
use IVIR3zaM\TalentTest\Models\MoModel;

class QueueTask extends Task
{
    /**
     * @var QueueInterface
     */
    protected $queue;

    /**
     * @var ManagerInterface
     */
    protected $manager;

    public function getQueue()
    {
        if (!$this->queue) {
            $this->setQueue($this->getDI()->getQueue());
        }
        return $this->queue;
    }

    public function setQueue(QueueInterface $queue)
    {
        $this->queue = $queue;
        return $this;
    }

    public function getManager()
    {
        if (!$this->manager) {
            $this->setManager($this->getDI()->getThread());
        }
        return $this->manager;
    }

    public function setManager(ManagerInterface $manager)
    {
        $this->manager = $manager;
        return $this;
    }

    public function handlerAction($params)
    {
        $concurrentItems = isset($params[0]) ? $params[0] : 10;
        $basePath = isset($params[1]) ? $params[1] : dirname(dirname(__DIR__));
        $file = $basePath . '/cache/queue.lock';
        $lock = $this->lockFile($file);
        if (!$lock) {
            echo 'Queue already running!' . PHP_EOL;
            return;
        }

        while ($this->getQueue()->count() > 0 && file_exists($file)) {
            $total = $this->getManager()->runCommand(PHP_BINARY . ' ' . $basePath . '/cli.php queue run ' . $concurrentItems . ' "' . $basePath . '"', 10);
            // detect if stopped due to overload
            if ($total < 10) {
                sleep(1);
            }
        }

        $this->unlockFile($lock);
        echo 'Done!' . PHP_EOL;
    }

    public function runAction($params)
    {
        $concurrentItems = isset($params[0]) ? $params[0] : 10;
        $basePath = isset($params[1]) ? $params[1] : dirname(dirname(__DIR__));
        while ($this->getQueue()->count() > 0 && $concurrentItems > 0) {
            $mo = $this->getQueue()->pop();
            $token = $this->getAuthToken($mo, $basePath);
            MoModel::convertToMoModel($mo, $token);
            $concurrentItems--;
        }
        echo 'Done!' . PHP_EOL;
    }

    public function countAction()
    {
        echo $this->getQueue()->count() . PHP_EOL;
    }

    public function clearAction()
    {
        echo 'clearing queue was ' . ($this->getQueue()->clear() ? 'SUCCESSFUL' : 'FAILED') . PHP_EOL;
    }

    protected function getAuthToken(MoInterface $mo, $basePath = '')
    {
        $json = json_encode($mo->getArray());
        return `{$basePath}/web/registermo {$json}`;
    }

    protected function lockFile($file = '')
    {
        if (file_exists($file)) {
            $type = 'r+';
        } else {
            $type = 'w+';
        }
        $fp = fopen($file, $type);
        if ($fp) {
            if (flock($fp, LOCK_EX | LOCK_NB)) {
                return $fp;
            }
        }
        return false;
    }

    protected function unlockFile($fp)
    {
        if ($fp) {
            flock($fp, LOCK_UN);
            fclose($fp);
        }
    }
}